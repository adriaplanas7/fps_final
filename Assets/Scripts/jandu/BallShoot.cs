﻿using UnityEngine;
using System.Collections;

public class BallShoot : MonoBehaviour {
    
    public GameObject bullet_prefab;
    float bulletImpulse = 20f;

    public AudioSource canon;

    
    // Update is called once per frame
    public void Shoot () {
        GameObject thebullet = (GameObject)Instantiate(bullet_prefab, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
        thebullet.GetComponent<Rigidbody>().AddForce( Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
        PlayCanonSound();
       
    }

    private void PlayCanonSound()
    {
            canon.Play();

    }
}
